<?php


Route::resource('/users', 'UserController');

Route::post('/users/restore', 'UserController@restore');

Route::resource('/posts', 'PostsController');

Route::resource('/tags', 'TagsController');

Route::get('/getTags', 'TagsController@getTags');

Route::get('/', 'DashController@index');
